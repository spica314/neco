#!/bin/bash

mkdir -p workspace

file="../stage0/test_codes/test_$1.neco"
echo "test ${file}"

../bin/neco_stage1_1 < ${file} > workspace/a.s
if [ $? -ne 0 ]; then
    echo "failed compile"
    exit 1
fi

as -o workspace/a.o workspace/a.s 
if [ $? -ne 0 ]; then
    echo "failed as"
    exit 1
fi

ld -s -o workspace/a.out workspace/a.o
if [ $? -ne 0 ]; then
    echo "failed ld"
    exit 1
fi

workspace/a.out
if [ $? -ne `cat ${file} | head -1 | cut -d " " -f 2` ]; then
    echo "failed run"
    exit 1
fi

echo "ok"
