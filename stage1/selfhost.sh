#!/bin/bash

mkdir -p workspace

../bin/neco_stage1_1 < ./main.neco > workspace/main.s
if [ $? -ne 0 ]; then
    echo "failed compile 1"
    exit 1
fi

as -o workspace/main.o workspace/main.s 
if [ $? -ne 0 ]; then
    echo "failed as 1"
    exit 1
fi

ld -s -o ../bin/neco_stage1_2 workspace/main.o
if [ $? -ne 0 ]; then
    echo "failed ld 1"
    exit 1
fi

../bin/neco_stage1_2 < ./main.neco > workspace/main2.s
if [ $? -ne 0 ]; then
    echo "failed compile 2"
    exit 1
fi

as -o workspace/main2.o workspace/main2.s 
if [ $? -ne 0 ]; then
    echo "failed as 2"
    exit 1
fi

ld -s -o ../bin/neco_stage1_3 workspace/main2.o
if [ $? -ne 0 ]; then
    echo "failed ld 2"
    exit 1
fi

cmp ../bin/neco_stage1_2 ../bin/neco_stage1_3
if [ $? -ne 0 ]; then
    echo "not equal"
    md5sum ../bin/*
    exit 1
fi

echo "ok"
