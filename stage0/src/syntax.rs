use crate::token::*;

#[derive(Debug, PartialEq, Eq)]
pub enum ParseError {
    Error,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Expr {
    BinaryApp(BinaryApp),
    Term(Term),
}

#[derive(Debug, PartialEq, Eq)]
pub enum Term {
    PrefixUnaryExpr(PrefixUnaryExpr),
    IfExpr(IfExpr),
    LoopExpr(LoopExpr),
    WhileExpr(WhileExpr),
    MatchExpr(MatchExpr),
    BlockExpr(BlockExpr),
}

#[derive(Debug, PartialEq, Eq)]
pub struct MatchPattern {
    pub enum_ident: Token,
    pub colon2: Token,
    pub value_ident: Token,
    pub arrow2: Token,
    pub expr: Box<Expr>,
    pub comma: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct MatchExpr {
    pub match_: Token,
    pub expr: Box<Expr>,
    pub lbrace: Token,
    pub patterns: Vec<MatchPattern>,
    pub rbrace: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct LoopExpr {
    pub loop_: Token,
    pub block_expr: BlockExpr,
}

#[derive(Debug, PartialEq, Eq)]
pub struct WhileExpr {
    pub while_: Token,
    pub cond_expr: Box<Expr>,
    pub block_expr: BlockExpr,
}

#[derive(Debug, PartialEq, Eq)]
pub struct IfExpr {
    pub if_: Token,
    pub expr1: Box<Expr>,
    pub then_: Token,
    pub expr2: Box<Expr>,
    pub else_: Token,
    pub expr3: Box<Expr>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct BinaryApp {
    pub op: Token,
    pub left: Box<Expr>,
    pub right: Box<Expr>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum PrefixUnaryExpr {
    RefApp(RefApp),
    DerefApp(DerefApp),
    NegateApp(NegateApp),
    FunApp(FunApp),
}

#[derive(Debug, PartialEq, Eq)]
pub enum Argument {
    RefAppInArg(RefAppInArg),
    DerefAppInArg(DerefAppInArg),
    NegateAppInArg(NegateAppInArg),
    PostfixUnaryExpr(PostfixUnaryExpr),
}

#[derive(Debug, PartialEq, Eq)]
pub struct FunApp {
    pub fun: PostfixUnaryExpr,
    pub args: Vec<Argument>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct RefApp {
    pub ref_: Token,
    pub prefix_unary_expr: Box<PrefixUnaryExpr>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct DerefApp {
    pub deref_: Token,
    pub prefix_unary_expr: Box<PrefixUnaryExpr>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct NegateApp {
    pub minus_: Token,
    pub prefix_unary_expr: Box<PrefixUnaryExpr>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct RefAppInArg {
    pub ref_: Token,
    pub argument: Box<Argument>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct DerefAppInArg {
    pub deref_: Token,
    pub argument: Box<Argument>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct NegateAppInArg {
    pub minus_: Token,
    pub argument: Box<Argument>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum PostfixUnaryExpr {
    IndexApp(IndexApp),
    DotApp(DotApp),
    Atom(Atom),
}

#[derive(Debug, PartialEq, Eq)]
pub struct DotApp {
    pub postfix_unary_expr: Box<PostfixUnaryExpr>,
    pub dot: Token,
    pub ident: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct IndexApp {
    pub postfix_unary_expr: Box<PostfixUnaryExpr>,
    pub lbracket: Token,
    pub expr: Box<Expr>,
    pub rbracket: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Atom {
    Number {
        token: Token,
    },
    Ident {
        token: Token,
    },
    Paren {
        lparen: Token,
        expr: Box<Expr>,
        rparen: Token,
    },
    Unit {
        token: Token,
    },
    EnumValue {
        enum_ident: Token,
        colon2: Token,
        value_ident: Token,
    },
    CharLiteral {
        token: Token,
    },
    StringLiteral {
        token: Token,
    },
}

#[derive(Debug, PartialEq, Eq)]
pub struct Let {
    pub token_let: Token,
    pub token_mut: Token,
    pub ident: Token,
    pub colon: Token,
    pub ty: Box<SynType>,
    pub equal: Option<Token>,
    pub expr: Option<Box<Expr>>,
    pub semicolon: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Assignment {
    pub lvalue: Box<Term>,
    pub equal: Token,
    pub expr: Box<Expr>,
    pub semicolon: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Statement {
    ExprWithSemicolon(Expr),
    Expr(Expr),
    Let(Box<Let>),
    Assignment(Assignment),
    Break(Break),
    Continue(Continue),
    Return(Return),
}

#[derive(Debug, PartialEq, Eq)]
pub struct Return {
    pub return_: Token,
    pub expr: Box<Expr>,
    pub semicolon: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Break {
    pub break_: Token,
    pub semicolon: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Continue {
    pub continue_: Token,
    pub semicolon: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct BlockExpr {
    pub lbrace: Token,
    pub statements: Vec<Statement>,
    pub rbrace: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub enum TopLevelStatement {
    FunDef(FunDef),
    StructDef(StructDef),
    EnumDef(EnumDef),
}

#[derive(Debug, PartialEq, Eq)]
pub struct Program {
    pub xs: Vec<TopLevelStatement>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Parameter {
    pub lparen: Token,
    pub ident: Token,
    pub colon: Token,
    pub ty: Box<SynType>,
    pub rparen: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct FunDef {
    pub fn_token: Token,
    pub name: Token,
    pub params: Vec<Parameter>,
    pub colon: Token,
    pub ret_type: Box<SynType>,
    pub equal: Token,
    pub expr: Box<Expr>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum SynType {
    PrimitiveType(PrimitiveType),
    SynArrayType(SynArrayType),
    RefType(RefType),
    UnitType(UnitType),
    AnyPtrType(AnyPtrType),
    AnyType(AnyType),
}

#[derive(Debug, PartialEq, Eq)]
pub struct UnitType {
    pub unit: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct AnyPtrType {
    pub any_ptr: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct AnyType {
    pub any: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct RefType {
    pub ampersand: Token,
    pub ty: Box<SynType>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct SynArrayType {
    pub lbracket: Token,
    pub ty: Box<SynType>,
    pub semicolon: Token,
    pub number: Token,
    pub rbracket: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct PrimitiveType {
    pub token: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct StructMember {
    pub name: Token,
    pub colon: Token,
    pub ty: Box<SynType>,
    pub comma: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct StructDef {
    pub struct_: Token,
    pub name: Token,
    pub lbrace: Token,
    pub members: Vec<StructMember>,
    pub rbrace: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct EnumMember {
    pub name: Token,
    pub comma: Token,
}

#[derive(Debug, PartialEq, Eq)]
pub struct EnumDef {
    pub enum_: Token,
    pub name: Token,
    pub lbrace: Token,
    pub members: Vec<EnumMember>,
    pub rbrace: Token,
}

fn parse_struct_member(ts: &[Token], i: &mut usize) -> Result<StructMember, ParseError> {
    let mut k = *i;
    let name = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let colon = parse_token(ts, &mut k, TokenKind::COLON)?;
    let ty = parse_type(ts, &mut k)?;
    let comma = parse_token(ts, &mut k, TokenKind::COMMA)?;
    *i = k;
    Ok(StructMember {
        name,
        colon,
        ty: Box::new(ty),
        comma,
    })
}

fn parse_struct_def(ts: &[Token], i: &mut usize) -> Result<StructDef, ParseError> {
    if *i >= ts.len() {
        return Err(ParseError::Error);
    }
    let mut k = *i;
    let struct_ = parse_token(ts, &mut k, TokenKind::STRUCT)?;
    let name = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let lbrace = parse_token(ts, &mut k, TokenKind::LBRACE)?;
    let mut members = vec![];
    while let Ok(res) = parse_struct_member(ts, &mut k) {
        members.push(res);
    }
    let rbrace = parse_token(ts, &mut k, TokenKind::RBRACE)?;
    *i = k;
    Ok(StructDef {
        struct_,
        name,
        lbrace,
        members,
        rbrace,
    })
}

fn parse_enum_member(ts: &[Token], i: &mut usize) -> Result<EnumMember, ParseError> {
    let mut k = *i;
    let name = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let comma = parse_token(ts, &mut k, TokenKind::COMMA)?;
    *i = k;
    Ok(EnumMember { name, comma })
}

fn parse_enum_def(ts: &[Token], i: &mut usize) -> Result<EnumDef, ParseError> {
    if *i >= ts.len() {
        return Err(ParseError::Error);
    }
    let mut k = *i;
    let enum_ = parse_token(ts, &mut k, TokenKind::ENUM)?;
    let name = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let lbrace = parse_token(ts, &mut k, TokenKind::LBRACE)?;
    let mut members = vec![];
    while let Ok(res) = parse_enum_member(ts, &mut k) {
        members.push(res);
    }
    let rbrace = parse_token(ts, &mut k, TokenKind::RBRACE)?;
    *i = k;
    Ok(EnumDef {
        enum_,
        name,
        lbrace,
        members,
        rbrace,
    })
}

fn parse_break(ts: &[Token], i: &mut usize) -> Result<Break, ParseError> {
    let mut k = *i;
    let break_ = parse_token(ts, &mut k, TokenKind::BREAK)?;
    let semicolon = parse_token(ts, &mut k, TokenKind::SEMICOLON)?;
    *i = k;
    Ok(Break { break_, semicolon })
}

fn parse_continue(ts: &[Token], i: &mut usize) -> Result<Continue, ParseError> {
    let mut k = *i;
    let continue_ = parse_token(ts, &mut k, TokenKind::CONTINUE)?;
    let semicolon = parse_token(ts, &mut k, TokenKind::SEMICOLON)?;
    *i = k;
    Ok(Continue {
        continue_,
        semicolon,
    })
}

fn parse_return(ts: &[Token], i: &mut usize) -> Result<Return, ParseError> {
    let mut k = *i;
    let return_ = parse_token(ts, &mut k, TokenKind::RETURN)?;
    let expr = parse_expr(ts, &mut k, None)?;
    let semicolon = parse_token(ts, &mut k, TokenKind::SEMICOLON)?;
    *i = k;
    Ok(Return {
        return_,
        expr: Box::new(expr),
        semicolon,
    })
}

fn parse_let(ts: &[Token], i: &mut usize) -> Result<Let, ParseError> {
    let mut k = *i;
    let token_let = parse_token(ts, &mut k, TokenKind::LET)?;
    let token_mut = parse_token(ts, &mut k, TokenKind::MUT)?;
    let ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let colon = parse_token(ts, &mut k, TokenKind::COLON)?;
    let ty = parse_type(ts, &mut k)?;
    let equal = parse_token(ts, &mut k, TokenKind::EQUAL)?;
    let expr = parse_expr(ts, &mut k, None)?;
    let semicolon = parse_token(ts, &mut k, TokenKind::SEMICOLON)?;
    *i = k;
    Ok(Let {
        token_let,
        token_mut,
        ident,
        colon,
        ty: Box::new(ty),
        equal: Some(equal),
        expr: Some(Box::new(expr)),
        semicolon,
    })
}

fn parse_uninitialized_let(ts: &[Token], i: &mut usize) -> Result<Let, ParseError> {
    let mut k = *i;
    let token_let = parse_token(ts, &mut k, TokenKind::LET)?;
    let token_mut = parse_token(ts, &mut k, TokenKind::MUT)?;
    let ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let colon = parse_token(ts, &mut k, TokenKind::COLON)?;
    let ty = parse_type(ts, &mut k)?;
    let semicolon = parse_token(ts, &mut k, TokenKind::SEMICOLON)?;
    *i = k;
    Ok(Let {
        token_let,
        token_mut,
        ident,
        colon,
        ty: Box::new(ty),
        equal: None,
        expr: None,
        semicolon,
    })
}

fn parse_assignment(ts: &[Token], i: &mut usize) -> Result<Assignment, ParseError> {
    let mut k = *i;
    let lvalue = parse_term(ts, &mut k)?;
    let equal = parse_token(ts, &mut k, TokenKind::EQUAL)?;
    let expr = parse_expr(ts, &mut k, None)?;
    let semicolon = parse_token(ts, &mut k, TokenKind::SEMICOLON)?;
    *i = k;
    Ok(Assignment {
        lvalue: Box::new(lvalue),
        equal,
        expr: Box::new(expr),
        semicolon,
    })
}

fn parse_statement(ts: &[Token], i: &mut usize) -> Result<Statement, ParseError> {
    if let Ok(res) = parse_let(ts, i) {
        return Ok(Statement::Let(Box::new(res)));
    } else if let Ok(res) = parse_uninitialized_let(ts, i) {
        return Ok(Statement::Let(Box::new(res)));
    } else if let Ok(res) = parse_assignment(ts, i) {
        return Ok(Statement::Assignment(res));
    } else if let Ok(res) = parse_break(ts, i) {
        return Ok(Statement::Break(res));
    } else if let Ok(res) = parse_continue(ts, i) {
        return Ok(Statement::Continue(res));
    } else if let Ok(res) = parse_return(ts, i) {
        return Ok(Statement::Return(res));
    }
    let mut k = *i;
    let mut ignore_parse_term = false;
    if let Ok(res) = parse_prefix_unary_expr(ts, &mut k) {
        let res = Expr::Term(Term::PrefixUnaryExpr(res));
        if parse_token(ts, &mut k, TokenKind::SEMICOLON).is_ok() {
            *i = k;
            return Ok(Statement::ExprWithSemicolon(res));
        } else if parse_token(ts, &mut k, TokenKind::RBRACE).is_ok() {
            *i = k - 1;
            return Ok(Statement::Expr(res));
        } else {
            ignore_parse_term = true;
        }
    }
    // if, match, loop, block only
    if !ignore_parse_term {
        let mut k = *i;
        if let Ok(res) = parse_term(ts, &mut k) {
            let res = Expr::Term(res);
            *i = k;
            return Ok(Statement::Expr(res));
        }
    }
    // expr
    let mut k = *i;
    if let Ok(res) = parse_expr(ts, &mut k, None) {
        if parse_token(ts, &mut k, TokenKind::SEMICOLON).is_ok() {
            *i = k;
            return Ok(Statement::ExprWithSemicolon(res));
        } else if parse_token(ts, &mut k, TokenKind::RBRACE).is_ok() {
            *i = k - 1;
            return Ok(Statement::Expr(res));
        }
    }
    Err(ParseError::Error)
}

fn parse_block_expr(ts: &[Token], i: &mut usize) -> Result<BlockExpr, ParseError> {
    let mut k = *i;
    let lbrace = parse_token(ts, &mut k, TokenKind::LBRACE)?;
    let statements = rep(ts, &mut k, |ts2, i2| parse_statement(ts2, i2))?;
    let rbrace = parse_token(ts, &mut k, TokenKind::RBRACE)?;
    *i = k;
    Ok(BlockExpr {
        lbrace,
        statements,
        rbrace,
    })
}

pub fn parse_program(ts: &[Token], i: &mut usize) -> Result<Program, ParseError> {
    let mut res = vec![];
    while let Ok(tls) = parse_top_level_statement(ts, i) {
        res.push(tls);
    }
    Ok(Program { xs: res })
}

pub fn parse_top_level_statement(
    ts: &[Token],
    i: &mut usize,
) -> Result<TopLevelStatement, ParseError> {
    if let Ok(fundef) = parse_fundef(ts, i) {
        return Ok(TopLevelStatement::FunDef(fundef));
    } else if let Ok(struct_def) = parse_struct_def(ts, i) {
        return Ok(TopLevelStatement::StructDef(struct_def));
    } else if let Ok(enum_def) = parse_enum_def(ts, i) {
        return Ok(TopLevelStatement::EnumDef(enum_def));
    }
    Err(ParseError::Error)
}

pub fn parse_token(
    ts: &[Token],
    i: &mut usize,
    token_kind: TokenKind,
) -> Result<Token, ParseError> {
    if *i >= ts.len() {
        return Err(ParseError::Error);
    } 
    if ts[*i].t == token_kind {
        let token = ts[*i].clone();
        *i += 1;
        return Ok(token);
    }
    Err(ParseError::Error)
}

pub fn parse_param(ts: &[Token], i: &mut usize) -> Result<Parameter, ParseError> {
    let mut k = *i;
    let lparen = parse_token(ts, &mut k, TokenKind::LPAREN)?;
    let ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let colon = parse_token(ts, &mut k, TokenKind::COLON)?;
    let ty = parse_type(ts, &mut k)?;
    let rparen = parse_token(ts, &mut k, TokenKind::RPAREN)?;
    *i = k;
    Ok(Parameter {
        lparen,
        ident,
        colon,
        ty: Box::new(ty),
        rparen,
    })
}

pub fn rep<F, T>(ts: &[Token], i: &mut usize, f: F) -> Result<Vec<T>, ParseError>
where
    F: Fn(&[Token], &mut usize) -> Result<T, ParseError>,
{
    let mut res = vec![];
    while let Ok(x) = f(ts, i) {
        res.push(x);
    }
    Ok(res)
}

pub fn parse_fundef(ts: &[Token], i: &mut usize) -> Result<FunDef, ParseError> {
    if *i >= ts.len() {
        return Err(ParseError::Error);
    }
    let mut k = *i;
    let token_fn = parse_token(ts, &mut k, TokenKind::FN)?;
    let name = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let params = if parse_token(ts, &mut k, TokenKind::UNIT).is_ok() {
        vec![]
    } else {
        rep(ts, &mut k, |ts, i| parse_param(ts, i))?
    };
    let colon = parse_token(ts, &mut k, TokenKind::COLON)?;
    let ret_type = parse_type(ts, &mut k)?;
    let equal = parse_token(ts, &mut k, TokenKind::EQUAL)?;
    let expr = parse_expr(ts, &mut k, None)?;
    *i = k;
    Ok(FunDef {
        fn_token: token_fn,
        name,
        params,
        colon,
        ret_type: Box::new(ret_type),
        equal,
        expr: Box::new(expr),
    })
}

pub fn parse_type(ts: &[Token], i: &mut usize) -> Result<SynType, ParseError> {
    fn parse_1(ts: &[Token], i: &mut usize) -> Result<SynType, ParseError> {
        let mut k = *i;
        let lbracket = parse_token(ts, &mut k, TokenKind::LBRACKET)?;
        let ty = parse_type(ts, &mut k)?;
        let semicolon = parse_token(ts, &mut k, TokenKind::SEMICOLON)?;
        let number = parse_token(ts, &mut k, TokenKind::Number)?;
        let rbracket = parse_token(ts, &mut k, TokenKind::RBRACKET)?;
        *i = k;
        Ok(SynType::SynArrayType(SynArrayType {
            lbracket,
            ty: Box::new(ty),
            semicolon,
            number,
            rbracket,
        }))
    }

    fn parse_2(ts: &[Token], i: &mut usize) -> Result<SynType, ParseError> {
        let mut k = *i;
        if ts[k].s != "&" {
            return Err(ParseError::Error);
        }
        let ampersand = ts[k].clone();
        k += 1;
        let ty = parse_type(ts, &mut k)?;
        *i = k;
        Ok(SynType::RefType(RefType {
            ampersand,
            ty: Box::new(ty),
        }))
    }

    if let Ok(res) = parse_1(ts, i) {
        Ok(res)
    } else if let Ok(res) = parse_2(ts, i) {
        Ok(res)
    } else if let Ok(res) = parse_token(ts, i, TokenKind::UNIT) {
        Ok(SynType::UnitType(UnitType { unit: res }))
    } else if let Ok(res) = parse_token(ts, i, TokenKind::ANYPTR) {
        Ok(SynType::AnyPtrType(AnyPtrType { any_ptr: res }))
    } else if let Ok(res) = parse_token(ts, i, TokenKind::ANY) {
        Ok(SynType::AnyType(AnyType { any: res }))
    } else {
        let token = ts[*i].clone();
        *i += 1;
        Ok(SynType::PrimitiveType(PrimitiveType { token }))
    }
}

pub fn parse(ts: &[Token]) -> Result<Expr, ParseError> {
    parse_expr(ts, &mut 0, None)
}

pub fn parse_atom(ts: &[Token], i: &mut usize) -> Result<Atom, ParseError> {
    fn parse_1(ts: &[Token], i: &mut usize) -> Result<Atom, ParseError> {
        let mut k = *i;
        let lparen = parse_token(ts, &mut k, TokenKind::LPAREN)?;
        let expr = parse_expr(ts, &mut k, None)?;
        let rparen = parse_token(ts, &mut k, TokenKind::RPAREN)?;
        *i = k;
        Ok(Atom::Paren {
            lparen,
            expr: Box::new(expr),
            rparen,
        })
    }

    fn parse_2(ts: &[Token], i: &mut usize) -> Result<Atom, ParseError> {
        let mut k = *i;
        let enum_ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
        let colon2 = parse_token(ts, &mut k, TokenKind::COLON2)?;
        let value_ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
        *i = k;
        Ok(Atom::EnumValue {
            enum_ident,
            colon2,
            value_ident,
        })
    }

    if let Ok(res) = parse_1(ts, i) {
        return Ok(res);
    } else if let Ok(res) = parse_2(ts, i) {
        return Ok(res);
    } else if let Ok(res) = parse_token(ts, i, TokenKind::Number) {
        return Ok(Atom::Number { token: res });
    } else if let Ok(res) = parse_token(ts, i, TokenKind::IDENT) {
        return Ok(Atom::Ident { token: res });
    } else if let Ok(res) = parse_token(ts, i, TokenKind::UNIT) {
        return Ok(Atom::Unit { token: res });
    } else if let Ok(res) = parse_token(ts, i, TokenKind::CHAR) {
        return Ok(Atom::CharLiteral { token: res });
    } else if let Ok(res) = parse_token(ts, i, TokenKind::STRING) {
        return Ok(Atom::StringLiteral { token: res });
    }
    Err(ParseError::Error)
}

pub fn parse_ref_app(ts: &[Token], i: &mut usize) -> Result<RefApp, ParseError> {
    let mut k = *i;
    if ts[k].s == "&" {
        let ref_ = ts[k].clone();
        k += 1;
        let prefix_unary_expr = parse_prefix_unary_expr(ts, &mut k)?;
        *i = k;
        Ok(RefApp {
            ref_,
            prefix_unary_expr: Box::new(prefix_unary_expr),
        })
    } else {
        Err(ParseError::Error)
    }
}

pub fn parse_deref_app(ts: &[Token], i: &mut usize) -> Result<DerefApp, ParseError> {
    let mut k = *i;
    if ts[k].s == "*" {
        let deref_ = ts[k].clone();
        k += 1;
        let prefix_unary_expr = parse_prefix_unary_expr(ts, &mut k)?;
        *i = k;
        Ok(DerefApp {
            deref_,
            prefix_unary_expr: Box::new(prefix_unary_expr),
        })
    } else {
        Err(ParseError::Error)
    }
}

pub fn parse_negate_app(ts: &[Token], i: &mut usize) -> Result<NegateApp, ParseError> {
    let mut k = *i;
    if ts[k].s == "-" {
        let minus_ = ts[k].clone();
        k += 1;
        let prefix_unary_expr = parse_prefix_unary_expr(ts, &mut k)?;
        *i = k;
        Ok(NegateApp {
            minus_,
            prefix_unary_expr: Box::new(prefix_unary_expr),
        })
    } else {
        Err(ParseError::Error)
    }
}

pub fn parse_prefix_unary_expr(ts: &[Token], i: &mut usize) -> Result<PrefixUnaryExpr, ParseError> {
    if let Ok(ref_app) = parse_ref_app(ts, i) {
        Ok(PrefixUnaryExpr::RefApp(ref_app))
    } else if let Ok(deref_app) = parse_deref_app(ts, i) {
        Ok(PrefixUnaryExpr::DerefApp(deref_app))
    } else if let Ok(negate_app) = parse_negate_app(ts, i) {
        Ok(PrefixUnaryExpr::NegateApp(negate_app))
    } else if let Ok(fun_app) = parse_fun_app(ts, i) {
        Ok(PrefixUnaryExpr::FunApp(fun_app))
    } else {
        Err(ParseError::Error)
    }
}

pub fn parse_ref_app_in_arg(ts: &[Token], i: &mut usize) -> Result<RefAppInArg, ParseError> {
    let mut k = *i;
    let ref_ = parse_token(ts, &mut k, TokenKind::REF)?;
    let argument = parse_argument(ts, &mut k)?;
    *i = k;
    Ok(RefAppInArg {
        ref_,
        argument: Box::new(argument),
    })
}

pub fn parse_deref_app_in_arg(ts: &[Token], i: &mut usize) -> Result<DerefAppInArg, ParseError> {
    let mut k = *i;
    let deref_ = parse_token(ts, &mut k, TokenKind::DEREF)?;
    let argument = parse_argument(ts, &mut k)?;
    *i = k;
    Ok(DerefAppInArg {
        deref_,
        argument: Box::new(argument),
    })
}

pub fn parse_negate_app_in_arg(ts: &[Token], i: &mut usize) -> Result<NegateAppInArg, ParseError> {
    let mut k = *i;
    let minus_ = parse_token(ts, &mut k, TokenKind::REF)?;
    let argument = parse_argument(ts, &mut k)?;
    *i = k;
    Ok(NegateAppInArg {
        minus_,
        argument: Box::new(argument),
    })
}

pub fn parse_argument(ts: &[Token], i: &mut usize) -> Result<Argument, ParseError> {
    if let Ok(ref_app) = parse_ref_app_in_arg(ts, i) {
        Ok(Argument::RefAppInArg(ref_app))
    } else if let Ok(deref_app) = parse_deref_app_in_arg(ts, i) {
        Ok(Argument::DerefAppInArg(deref_app))
    } else if let Ok(negate_app) = parse_negate_app_in_arg(ts, i) {
        Ok(Argument::NegateAppInArg(negate_app))
    } else if let Ok(postfix_unary_expr) = parse_postfix_unary_expr(ts, i) {
        Ok(Argument::PostfixUnaryExpr(postfix_unary_expr))
    } else {
        Err(ParseError::Error)
    }
}

pub fn parse_postfix_unary_expr(
    ts: &[Token],
    i: &mut usize,
) -> Result<PostfixUnaryExpr, ParseError> {
    struct Index {
        pub lbracket: Token,
        pub expr: Box<Expr>,
        pub rbracket: Token,
    }

    struct Dot {
        pub dot: Token,
        pub ident: Token,
    }

    fn parse_index(ts: &[Token], i: &mut usize) -> Result<Index, ParseError> {
        let mut k = *i;
        let lbracket = parse_token(ts, &mut k, TokenKind::LBRACKET)?;
        let expr = parse_expr(ts, &mut k, None)?;
        let rbracket = parse_token(ts, &mut k, TokenKind::RBRACKET)?;
        *i = k;
        Ok(Index {
            lbracket,
            expr: Box::new(expr),
            rbracket,
        })
    }

    fn parse_dot(ts: &[Token], i: &mut usize) -> Result<Dot, ParseError> {
        let mut k = *i;
        let dot = parse_token(ts, &mut k, TokenKind::DOT)?;
        let ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
        *i = k;
        Ok(Dot { dot, ident })
    }

    let mut k = *i;
    let atom = parse_atom(ts, &mut k)?;
    let mut postfix_unary_expr = PostfixUnaryExpr::Atom(atom);
    loop {
        if let Ok(index) = parse_index(ts, &mut k) {
            postfix_unary_expr = PostfixUnaryExpr::IndexApp(IndexApp {
                postfix_unary_expr: Box::new(postfix_unary_expr),
                lbracket: index.lbracket,
                expr: index.expr,
                rbracket: index.rbracket,
            });
        } else if let Ok(dot) = parse_dot(ts, &mut k) {
            postfix_unary_expr = PostfixUnaryExpr::DotApp(DotApp {
                postfix_unary_expr: Box::new(postfix_unary_expr),
                dot: dot.dot,
                ident: dot.ident,
            });
        } else {
            break;
        }
    }
    *i = k;
    Ok(postfix_unary_expr)
}

pub fn parse_fun_app(ts: &[Token], i: &mut usize) -> Result<FunApp, ParseError> {
    let mut k = *i;
    let fun = parse_postfix_unary_expr(ts, &mut k)?;
    let mut args = vec![];
    while let Ok(argument) = parse_argument(ts, &mut k) {
        args.push(argument);
    }
    *i = k;
    Ok(FunApp { fun, args })
}

pub fn parse_match_pattern(ts: &[Token], i: &mut usize) -> Result<MatchPattern, ParseError> {
    let mut k = *i;
    let enum_ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let colon2 = parse_token(ts, &mut k, TokenKind::COLON2)?;
    let value_ident = parse_token(ts, &mut k, TokenKind::IDENT)?;
    let arrow2 = parse_token(ts, &mut k, TokenKind::ARROW2)?;
    let expr = parse_expr(ts, &mut k, None)?;
    let comma = parse_token(ts, &mut k, TokenKind::COMMA)?;
    *i = k;
    Ok(MatchPattern {
        enum_ident,
        colon2,
        value_ident,
        arrow2,
        expr: Box::new(expr),
        comma,
    })
}

pub fn parse_match_expr(ts: &[Token], i: &mut usize) -> Result<MatchExpr, ParseError> {
    let mut k = *i;
    let match_ = parse_token(ts, &mut k, TokenKind::MATCH)?;
    let expr = parse_expr(ts, &mut k, None)?;
    let lbrace = parse_token(ts, &mut k, TokenKind::LBRACE)?;
    let mut patterns = vec![];
    while let Ok(res) = parse_match_pattern(ts, &mut k) {
        patterns.push(res);
    }
    let rbrace = parse_token(ts, &mut k, TokenKind::RBRACE)?;
    *i = k;
    Ok(MatchExpr {
        match_,
        expr: Box::new(expr),
        lbrace,
        patterns,
        rbrace,
    })
}

pub fn parse_loop_expr(ts: &[Token], i: &mut usize) -> Result<LoopExpr, ParseError> {
    let mut k = *i;
    let loop_ = parse_token(ts, &mut k, TokenKind::LOOP)?;
    let block_expr = parse_block_expr(ts, &mut k)?;
    *i = k;
    Ok(LoopExpr { loop_, block_expr })
}

pub fn parse_while_expr(ts: &[Token], i: &mut usize) -> Result<WhileExpr, ParseError> {
    let mut k = *i;
    let while_ = parse_token(ts, &mut k, TokenKind::WHILE)?;
    let cond_expr = parse_expr(ts, &mut k, None)?;
    let block_expr = parse_block_expr(ts, &mut k)?;
    *i = k;
    Ok(WhileExpr { while_, cond_expr: Box::new(cond_expr), block_expr })
}

pub fn parse_if_expr(ts: &[Token], i: &mut usize) -> Result<IfExpr, ParseError> {
    let mut k = *i;
    let if_ = parse_token(ts, &mut k, TokenKind::IF)?;
    let expr1 = parse_expr(ts, &mut k, None)?;
    let then_ = parse_token(ts, &mut k, TokenKind::THEN)?;
    let expr2 = parse_expr(ts, &mut k, None)?;
    let else_ = parse_token(ts, &mut k, TokenKind::ELSE)?;
    let expr3 = parse_expr(ts, &mut k, None)?;
    *i = k;
    Ok(IfExpr {
        if_,
        expr1: Box::new(expr1),
        then_,
        expr2: Box::new(expr2),
        else_,
        expr3: Box::new(expr3),
    })
}

pub fn parse_term(ts: &[Token], i: &mut usize) -> Result<Term, ParseError> {
    if let Ok(if_expr) = parse_if_expr(ts, i) {
        Ok(Term::IfExpr(if_expr))
    } else if let Ok(loop_expr) = parse_loop_expr(ts, i) {
        Ok(Term::LoopExpr(loop_expr))
    } else if let Ok(while_expr) = parse_while_expr(ts, i) {
        Ok(Term::WhileExpr(while_expr))
    } else if let Ok(match_expr) = parse_match_expr(ts, i) {
        Ok(Term::MatchExpr(match_expr))
    } else if let Ok(block_expr) = parse_block_expr(ts, i) {
        Ok(Term::BlockExpr(block_expr))
    } else if let Ok(prefix_unary_expr) = parse_prefix_unary_expr(ts, i) {
        Ok(Term::PrefixUnaryExpr(prefix_unary_expr))
    } else {
        Err(ParseError::Error)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum AssocKind {
    Left,
    None,
    Right,
}

pub fn op_precedence(token: &Token) -> (i64, AssocKind) {
    match token.t {
        TokenKind::AND => (20, AssocKind::Left),
        TokenKind::OR  => (20, AssocKind::Left),
        TokenKind::EQ  => (30, AssocKind::None),
        TokenKind::NEQ => (30, AssocKind::None),
        TokenKind::LT  => (30, AssocKind::None),
        TokenKind::LEQ => (30, AssocKind::None),
        TokenKind::GT  => (30, AssocKind::None),
        TokenKind::GEQ => (30, AssocKind::None),
        TokenKind::ADD => (40, AssocKind::Left),
        TokenKind::SUB => (40, AssocKind::Left),
        TokenKind::MUL => (50, AssocKind::Left),
        TokenKind::DIV => (50, AssocKind::Left),
        TokenKind::MOD => (50, AssocKind::Left),
        _ => (0, AssocKind::None),
    }
}

pub fn parse_expr(
    ts: &[Token],
    i: &mut usize,
    prev_prec: Option<(AssocKind, i64)>,
) -> Result<Expr, ParseError> {
    let mut res = if let Ok(term) = parse_term(ts, i) {
        Expr::Term(term)
    } else {
        return Err(ParseError::Error);
    };
    while *i < ts.len() {
        let (precedence, assoc) = op_precedence(&ts[*i]);
        if precedence == 0 {
            break;
        }
        if let Some(prev_prec) = prev_prec {
            match prev_prec.0 {
                AssocKind::Left => {
                    if precedence <= prev_prec.1 {
                        break;
                    }
                }
                AssocKind::Right => {
                    if precedence < prev_prec.1 {
                        break;
                    }
                }
                AssocKind::None => {
                    if precedence < prev_prec.1 {
                        break;
                    }
                }
            }
        }
        let token = ts[*i].clone();
        *i += 1;
        let right = parse_expr(ts, i, Some((assoc, precedence)))
            .unwrap_or_else(|_| panic!("ts[i..] = {:?}", &ts[*i..]));
        res = Expr::BinaryApp(BinaryApp {
            op: token.clone(),
            left: Box::new(res),
            right: Box::new(right),
        });
    }
    Ok(res)
}
