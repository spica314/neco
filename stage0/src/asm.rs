use crate::syntax::*;
use crate::types::*;
use crate::token::*;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Address {
    Stack { offset: i64 },
    Global { name: String },
    Pointer { reg: Reg },
}

impl Address {
    pub fn to_asm(&self) -> String {
        match self {
            Address::Stack { offset } => format!("[rbp+{}]", offset).to_string(),
            Address::Global { name } => name.clone(),
            Address::Pointer { reg } => format!("[{}]", reg.as_str()).to_string(),
        }
    }
}

#[derive(Default)]
pub struct OffsetManager {
    arg_offset: i64,
    local_offset: i64,
    history: Vec<(i64, i64)>,
}

impl OffsetManager {
    pub fn new() -> OffsetManager {
        OffsetManager {
            arg_offset: 8,
            local_offset: 0,
            history: vec![],
        }
    }
    pub fn get_arg_offset(&mut self, memsize: i64) -> i64 {
        self.arg_offset += memsize;
        while self.arg_offset % 8 != 0 {
            self.arg_offset += 1;
        }
        self.arg_offset
    }
    pub fn get_local_offset(&mut self, memsize: i64) -> (i64, i64) {
        let old = self.local_offset;
        self.local_offset -= memsize;
        while self.local_offset % 8 != 0 {
            self.local_offset -= 1;
        }
        (self.local_offset, old - self.local_offset)
    }
    pub fn enter_block(&mut self) {
        self.history.push((self.arg_offset, self.local_offset));
    }
    pub fn leave_block(&mut self) {
        let (arg_offset, local_offset) = self.history.pop().unwrap();
        self.arg_offset = arg_offset;
        self.local_offset = local_offset;
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Entry {
    s: String,
    addr: Address,
    ty: Type,
    is_function_entry_point: bool,
}

#[derive(Debug, PartialEq, Eq)]
pub enum SymbolTableEntry {
    Entry(Entry),
    EnterFunction,
    LeaveFunction,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Reg {
    RAX,
    EAX,
    AX,
    AL,
    RSP,
    RDI,
    DIL,
    RDX,
    BL,
    BX,
    EBX,
    RBX,
    R15,
}

impl Reg {
    pub fn as_str(&self) -> &str {
        match self {
            Reg::RAX => "rax",
            Reg::EAX => "eax",
            Reg::AX => "ax",
            Reg::AL => "al",
            Reg::RSP => "rsp",
            Reg::RDI => "rdi",
            Reg::DIL => "dil",
            Reg::RDX => "rdx",
            Reg::BL => "bl",
            Reg::BX => "bx",
            Reg::EBX => "ebx",
            Reg::RBX => "rbx",
            Reg::R15 => "r15",
        }
    }
}

#[derive(Default)]
pub struct SymbolTable {
    xs: Vec<SymbolTableEntry>,
}

impl SymbolTable {
    pub fn new() -> SymbolTable {
        SymbolTable { xs: vec![] }
    }
    pub fn enter_func(&mut self) {
        self.xs.push(SymbolTableEntry::EnterFunction);
    }
    pub fn leave_func(&mut self) {
        self.xs.push(SymbolTableEntry::LeaveFunction);
    }
    pub fn push(&mut self, x: Entry) {
        self.xs.push(SymbolTableEntry::Entry(x));
    }
    pub fn get_entry(&self, s: &str) -> Option<&Entry> {
        let mut skip_stack = vec![];
        for entry in self.xs.iter().rev() {
            if skip_stack.is_empty() {
                match entry {
                    SymbolTableEntry::Entry(entry) => {
                        if entry.s == s {
                            return Some(entry);
                        }
                    }
                    SymbolTableEntry::EnterFunction => {
                        // pass when in function
                    }
                    SymbolTableEntry::LeaveFunction => {
                        skip_stack.push(SymbolTableEntry::LeaveFunction);
                    }
                }
            } else {
                match entry {
                    SymbolTableEntry::EnterFunction => {
                        if skip_stack[skip_stack.len() - 1] == SymbolTableEntry::LeaveFunction {
                            skip_stack.pop();
                        } else {
                            panic!("entry = {:?}", entry);
                        }
                    }
                    SymbolTableEntry::LeaveFunction => {
                        skip_stack.push(SymbolTableEntry::LeaveFunction);
                    }
                    _ => {}
                }
            }
        }
        None
    }
}

pub fn memsize_of_type(ty: &Type, type_table: &TypeTable) -> i64 {
    match ty {
        Type::Unit => 0,
        Type::I(n_bits) => match n_bits {
            8 => 1,
            64 => 8,
            _ => panic!("n_bits = {:?}", n_bits),
        },
        Type::Arrow(_from, _to) => unimplemented!(),
        Type::Array(ty, num) => num * memsize_of_type(ty, type_table),
        Type::Ref(_) => 8,
        Type::UserType(type_name) => type_table.get_size(type_name).expect(&format!("type_name = {:?}", type_name)),
        Type::AnyPtr => 8,
        Type::Any => 8,
    }
}

#[derive(Debug)]
pub struct StructMemberInfo {
    name: String,
    ty: Type,
    offset: i64,
}

#[derive(Debug)]
pub struct StructInfo {
    name: String,
    members: Vec<StructMemberInfo>,
    size: i64,
}

#[derive(Debug)]
pub struct EnumMemberInfo {
    pub name: String,
    pub value: i64,
}

#[derive(Debug)]
pub struct EnumInfo {
    name: String,
    members: Vec<EnumMemberInfo>,
}

#[derive(Debug)]
pub struct TypeTable {
    structs: Vec<StructInfo>,
    enums: Vec<EnumInfo>,
}

impl TypeTable {
    pub fn new() -> TypeTable {
        TypeTable {
            structs: vec![],
            enums: vec![],
        }
    }
    pub fn get_struct_member_info(&self, t: &Type, m: &String) -> Option<&StructMemberInfo> {
        if let Type::Ref(r) = t {
            return self.get_struct_member_info(r, m);
        } else if let Type::UserType(struct_name) = t {
            for struct_ in &self.structs {
                if &struct_.name == struct_name {
                    for member in &struct_.members {
                        if &member.name == m {
                            return Some(member);
                        }
                    }
                }
            }
        }
        None
    }
    pub fn get_enum_member_info(&self, t: &Type, m: &String) -> Option<&EnumMemberInfo> {
        if let Type::Ref(r) = t {
            return self.get_enum_member_info(r, m);
        } else if let Type::UserType(enum_name) = t {
            for enum_ in &self.enums {
                if &enum_.name == enum_name {
                    for member in &enum_.members {
                        if &member.name == m {
                            return Some(member);
                        }
                    }
                }
            }
        }
        None
    }
    pub fn get_size(&self, s: &String) -> Option<i64> {
        for struct_ in &self.structs {
            if &struct_.name == s {
                return Some(struct_.size);
            }
        }
        for enum_ in &self.enums {
            if &enum_.name == s {
                return Some(8);
            }
        }
        None
    }
    pub fn push_struct(&mut self, struct_def: &StructDef) {
        let mut members = vec![];
        let mut next_offset = 0;
        for member in &struct_def.members {
            let ty = Type::from_syntype(&member.ty);
            let size = memsize_of_type(&ty, self);
            while next_offset % std::cmp::min(8, size) != 0 {
                next_offset += 1;
            }
            members.push(StructMemberInfo {
                name: member.name.s.clone(),
                ty: Type::from_syntype(&member.ty),
                offset: next_offset,
            });
            next_offset += size;
        }
        while next_offset % 8 != 0 {
            next_offset += 1;
        }
        self.structs.push(StructInfo {
            name: struct_def.name.s.clone(),
            members: members,
            size: next_offset,
        })
    }
    pub fn push_enum(&mut self, enum_def: &EnumDef) {
        let mut members = vec![];
        let mut next_value = 0;
        for member in &enum_def.members {
            members.push(EnumMemberInfo {
                name: member.name.s.clone(),
                value: next_value,
            });
            next_value += 1;
        }
        self.enums.push(EnumInfo {
            name: enum_def.name.s.clone(),
            members: members,
        })
    }
}

pub struct StringTable {
    pub xs: Vec<(String, Token)>,
}

impl StringTable {
    fn new() -> StringTable {
        StringTable {
            xs: vec![],
        }
    }
    fn push(&mut self, label: &String, token: &Token) {
        self.xs.push((label.clone(), token.clone()));
    }
}

pub struct Env {
    pub asm: String,
    pub label_index: i64,
    pub loop_label_stack: Vec<(String, String)>,
    pub table: SymbolTable,
    pub offset_manager: OffsetManager,
    pub type_table: TypeTable,
    pub string_table: StringTable,
}

impl Env {
    pub fn new(
        table: SymbolTable,
        offset_manager: OffsetManager,
        type_table: TypeTable,
        string_table: StringTable,
    ) -> Env {
        Env {
            asm: String::new(),
            label_index: 1,
            loop_label_stack: vec![],
            table,
            offset_manager,
            type_table,
            string_table,
        }
    }
    pub fn gen_label(&mut self) -> String {
        let res = format!("LABEL_{}", self.label_index).to_string();
        self.label_index += 1;
        res
    }
    pub fn push_loop_label(&mut self, begin: &String, end: &String) {
        self.loop_label_stack.push((begin.clone(), end.clone()));
    }
    pub fn pop_loop_label(&mut self) {
        self.loop_label_stack.pop();
    }
}

trait GenAsmExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type;
    fn gen_asm_dlvalue(&self, env: &mut Env) -> Type {
        let mut ty = self.gen_asm_lvalue(env);
        while let Type::Ref(ty2) = ty {
            ty = *ty2.clone();
            env.asm.push_str("    pop rax\n");
            env.asm.push_str("    mov rax, [rax]\n");
            env.asm.push_str("    push rax\n");
        }
        ty
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type;
}

// template
/*
impl GenAsmExpr for BlockExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        unimplemented!()
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        unimplemented!()
    }
}
*/

impl GenAsmExpr for BlockExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        let mut ty = Type::Unit;
        env.offset_manager.enter_block();
        for statement in &self.statements {
            match statement {
                Statement::ExprWithSemicolon(expr) => {
                    expr.gen_asm_lvalue(env);
                }
                Statement::Expr(expr) => {
                    ty = expr.gen_asm_lvalue(env);
                }
                Statement::Let(let_) => {
                    let ty = Type::from_syntype(let_.ty.as_ref());
                    let memsize = memsize_of_type(&ty, &env.type_table);
                    let (offset, shift) = env.offset_manager.get_local_offset(memsize);
                    assert!(let_.equal.is_some() == let_.expr.is_some());
                    if let Some(expr) = &let_.expr {
                        let right_ty = expr.gen_asm_rvalue(env);
                        if ! Type::assignable(&ty, &right_ty) {
                            panic!("type error, let = {:?}", let_);
                        }
                        env.asm.push_str("    pop rax\n");
                        match memsize {
                            1 => {
                                env.asm.push_str(&format!("    al, [rbp+{}]\n", offset));
                            }
                            8 => {
                                env.asm.push_str(&format!("    rax, [rbp+{}]\n", offset));
                            }
                            _ => {}
                        }
                    }
                    env.asm.push_str(&format!("    sub rsp, {}\n", shift));
                    env.table.push(Entry {
                        s: let_.ident.s.clone(),
                        addr: Address::Stack { offset },
                        ty: ty.clone(),
                        is_function_entry_point: false,
                    });
                }
                Statement::Assignment(assignment) => {
                    let right_ty = assignment.expr.gen_asm_rvalue(env);
                    let ty = assignment.lvalue.gen_asm_lvalue(env);
                    if ! Type::assignable(&ty, &right_ty) {
                        panic!("type error, assignment = {:?}", assignment);
                    }
                    env.asm.push_str("    pop rax\n");
                    env.asm.push_str("    pop rbx\n");
                    match ty {
                        Type::Ref(ty) => match memsize_of_type(&ty, &env.type_table) {
                            1 => {
                                env.asm.push_str(&format!("    mov [rax], bl\n"));
                            }
                            8 => {
                                env.asm.push_str(&format!("    mov [rax], rbx\n"));
                            }
                            _fail => panic!("memsize = {:?}", _fail),
                        },
                        _ => panic!("ty = {:?}", ty),
                    }
                }
                Statement::Break(break_) => {
                    env.asm.push_str(&format!("    jmp {}\n", env.loop_label_stack.last().unwrap().1));
                }
                Statement::Continue(continue_) => {
                    env.asm.push_str(&format!("    jmp {}\n", env.loop_label_stack.last().unwrap().0));
                }
                Statement::Return(return_) => {
                    panic!("self = {:?}", self);
                }
            }
        }
        env.offset_manager.leave_block();
        ty
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let mut ty = Type::Unit;
        env.offset_manager.enter_block();
        for statement in &self.statements {
            match statement {
                Statement::ExprWithSemicolon(expr) => {
                    expr.gen_asm_rvalue(env);
                }
                Statement::Expr(expr) => {
                    ty = expr.gen_asm_rvalue(env);
                }
                Statement::Let(let_) => {
                    let ty = Type::from_syntype(let_.ty.as_ref());
                    let memsize = memsize_of_type(&ty, &env.type_table);
                    let (offset, shift) = env.offset_manager.get_local_offset(memsize);
                    assert!(let_.equal.is_some() == let_.expr.is_some());
                    if let Some(expr) = &let_.expr {
                        let right_ty = expr.gen_asm_rvalue(env);
                        if ! Type::assignable(&ty, &right_ty) {
                            panic!("type error, let = {:?}, ty = {:?}, right_ty = {:?}", let_, ty, right_ty);
                        }
                        env.asm.push_str("    pop rax\n");
                        match memsize {
                            1 => {
                                env.asm.push_str(&format!("    mov [rbp+{}], al\n", offset));
                            }
                            8 => {
                                env.asm.push_str(&format!("    mov [rbp+{}], rax\n", offset));
                            }
                            _ => {}
                        }
                    }
                    env.asm.push_str(&format!("    sub rsp, {}\n", shift));
                    env.table.push(Entry {
                        s: let_.ident.s.clone(),
                        addr: Address::Stack { offset },
                        ty: ty.clone(),
                        is_function_entry_point: false,
                    });
                }
                Statement::Assignment(assignment) => {
                    let right_ty = assignment.expr.gen_asm_rvalue(env);
                    let ty = assignment.lvalue.gen_asm_lvalue(env);
                    if ! Type::assignable(&ty, &right_ty) {
                        panic!("type error, assignment = {:?}, ty = {:?}, right_ty = {:?}", assignment, ty, right_ty);
                    }
                    env.asm.push_str("    pop rax\n"); // lvalue
                    env.asm.push_str("    pop rbx\n"); // rvalue
                    match memsize_of_type(&ty, &env.type_table) {
                        1 => {
                            env.asm.push_str(&format!("    mov [rax], bl\n"));
                        }
                        8 => {
                            env.asm.push_str(&format!("    mov [rax], rbx\n"));
                        }
                        _fail => panic!("_fail = {:?}", _fail),
                    }
                }
                Statement::Break(break_) => {
                    env.asm.push_str(&format!("    jmp {}\n", env.loop_label_stack.last().unwrap().1));
                }
                Statement::Continue(continue_) => {
                    env.asm.push_str(&format!("    jmp {}\n", env.loop_label_stack.last().unwrap().0));
                }
                Statement::Return(return_) => {
                    return_.expr.gen_asm_rvalue(env);
                    env.asm.push_str("    pop rax\n");
                    env.asm.push_str("    mov rsp, rbp\n");
                    env.asm.push_str("    pop rbp\n");
                    env.asm.push_str("    ret\n");
                }
            }
        }
        env.offset_manager.leave_block();
        ty
    }
}

pub fn gen_asm_fun_def(fun_def: &FunDef, env: &mut Env) {
    env.offset_manager = OffsetManager::new();
    // enter
    env.table.enter_func();
    let mut offset_manager = OffsetManager::new();
    // args
    for param in fun_def.params.iter().rev() {
        let ty = Type::from_syntype(param.ty.as_ref());
        let memsize = memsize_of_type(&ty, &env.type_table);
        assert!(memsize != 0);
        assert!(memsize == 8, "fun_def = {:?}", fun_def);
        let offset = offset_manager.get_arg_offset(memsize);
        env.table.push(Entry {
            s: param.ident.s.clone(),
            addr: Address::Stack { offset },
            ty,
            is_function_entry_point: false,
        });
    }
    // label
    env.asm.push_str(&format!("{}:\n", fun_def.name.s));
    // prologue
    env.asm.push_str("    push rbp\n");
    env.asm.push_str("    mov rbp, rsp\n");
    // body
    fun_def.expr.gen_asm_rvalue(env);
    env.asm.push_str("    pop rax\n");
    // epilogue
    env.asm.push_str("    mov rsp, rbp\n");
    env.asm.push_str("    pop rbp\n");
    env.asm.push_str("    ret\n");
    // pop
    env.table.leave_func();
}

pub fn gen_asm_program(program: &Program) -> String {
    let mut table = SymbolTable::new();
    let mut type_table = TypeTable::new();
    let any = Box::new(Type::Any);
    table.push(Entry {
        s: "_syscall".to_string(),
        addr: Address::Global {
            name: "_syscall".to_string(),
        },
        ty: Type::Arrow(any.clone(),
            Box::new(Type::Arrow(any.clone(),
            Box::new(Type::Arrow(any.clone(),
            Box::new(Type::Arrow(any.clone(),
            Box::new(Type::Arrow(any.clone(),
            Box::new(Type::Arrow(any.clone(),
            Box::new(Type::Arrow(any.clone(), any.clone()))))))))))))),
        is_function_entry_point: true,
    });
    table.push(Entry {
        s: "_to_ptr".to_string(),
        addr: Address::Global {
            name: "_to_ptr".to_string(),
        },
        ty: Type::Arrow(any, Box::new(Type::AnyPtr)),
        is_function_entry_point: true,
    });
    table.push(Entry {
        s: "_i8_to_i64".to_string(),
        addr: Address::Global {
            name: "_i8_to_i64".to_string(),
        },
        ty: Type::Arrow(Box::new(Type::I(8)), Box::new(Type::I(64))),
        is_function_entry_point: true,
    });
    table.push(Entry {
        s: "_i64_to_i8".to_string(),
        addr: Address::Global {
            name: "_i64_to_i8".to_string(),
        },
        ty: Type::Arrow(Box::new(Type::I(64)), Box::new(Type::I(8))),
        is_function_entry_point: true,
    });
    for tls in program.xs.iter() {
        match tls {
            TopLevelStatement::FunDef(fun_def) => {
                let mut ty = Type::from_syntype(&fun_def.ret_type);
                if fun_def.params.is_empty() {
                    ty = Type::Arrow(Box::new(Type::Unit), Box::new(ty));
                }
                else {
                    for param in fun_def.params.iter().rev() {
                        let ty2 = Type::from_syntype(&param.ty);
                        ty = Type::Arrow(Box::new(ty2), Box::new(ty));
                    }
                }
                table.push(Entry {
                    s: fun_def.name.s.clone(),
                    addr: Address::Global {
                        name: fun_def.name.s.clone(),
                    },
                    ty,
                    is_function_entry_point: true,
                })
            }
            TopLevelStatement::StructDef(struct_def) => {
                type_table.push_struct(struct_def);
            }
            TopLevelStatement::EnumDef(enum_def) => {
                type_table.push_enum(enum_def);
            }
        }
    }
    let mut offset_manager = OffsetManager::new();
    let mut string_table = StringTable::new();
    let mut env = Env::new(table, offset_manager, type_table, string_table);

    env.asm.push_str(
        r#"
.intel_syntax noprefix
_syscall:
    push rbp
    mov rbp, rsp
    mov r9, [rbp+16]
    mov r8, [rbp+24]
    mov r10, [rbp+32]
    mov rdx, [rbp+40]
    mov rsi, [rbp+48]
    mov rdi, [rbp+56]
    mov rax, [rbp+64]
    syscall
    mov rsp, rbp
    pop rbp
    ret
_to_ptr:
    push rbp
    mov rbp, rsp
    mov rax, [rbp+16]
    mov rsp, rbp
    pop rbp
    ret
_i8_to_i64:
    push rbp
    mov rbp, rsp
    mov al, [rbp+16]
    movsx rax, al
    mov rsp, rbp
    pop rbp
    ret
_i64_to_i8:
    push rbp
    mov rbp, rsp
    mov rax, [rbp+16]
    mov rsp, rbp
    pop rbp
    ret

    .global _start
_start:
    pushfq
    pop rax
    mov rdi, 0x0000000000040000
    or rax, rdi
    push rax
    popfq
    call main
    mov rdi, rax
    mov rax, 60
    syscall
"#,
    );


    for tls in program.xs.iter() {
        match tls {
            TopLevelStatement::FunDef(fun_def) => {
                gen_asm_fun_def(fun_def, &mut env);
            }
            TopLevelStatement::StructDef(_) => {}
            TopLevelStatement::EnumDef(_) => {}
        }
    }
    // strings
    env.asm.push_str("    .section .data\n");
    for (label, token) in &env.string_table.xs {
        env.asm.push_str(&format!("{}:", label));
        let s: String = token.s.chars().skip(1).collect();
        env.asm.push_str(&format!("    .asciz {}\n", s));
    }
    // ret
    env.asm
}

impl GenAsmExpr for Atom {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        match self {
            Atom::Number { .. } => {
                panic!("atom = {:?}", self);
            }
            Atom::Ident { token } => {
                if let Some(entry) = env.table.get_entry(&token.s) {
                    if entry.is_function_entry_point {
                        panic!("atom = {:?}, entry = {:?}", self, entry);
                    } else {
                        env.asm.push_str(&format!("    lea rax, {}\n", entry.addr.to_asm()));
                        env.asm.push_str("    push rax\n");
                        entry.ty.clone()
                    }
                } else {
                    panic!("atom = {:?}", self);
                }
            }
            Atom::Paren { expr, .. } => expr.gen_asm_lvalue(env),
            Atom::Unit { .. } => {
                panic!("atom = {:?}", self);
            }
            Atom::EnumValue { .. } => {
                panic!("atom = {:?}", self);
            }
            Atom::CharLiteral { .. } => {
                panic!("atom = {:?}", self);
            }
            Atom::StringLiteral { .. } => {
                panic!("atom = {:?}", self);
            }
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        match self {
            Atom::Number { token } => {
                env.asm.push_str(&format!("    mov rax, {}\n", token.s.parse::<i64>().unwrap()));
                env.asm.push_str("    push rax\n");
                // TODO: NUMBER type
                Type::Any
            }
            Atom::Ident { token } => {
                if let Some(entry) = env.table.get_entry(&token.s) {
                    if entry.is_function_entry_point {
                        env.asm.push_str(&format!("    lea rax, {}\n", entry.addr.to_asm()));
                        env.asm.push_str("    push rax\n");
                    } else {
                        match memsize_of_type(&entry.ty, &env.type_table) {
                            0 => {}
                            1 => {
                                env.asm.push_str(&format!("    mov al, {}\n", entry.addr.to_asm()));
                                env.asm.push_str("    movsx rax, al\n");
                                env.asm.push_str("    push rax\n");
                            }
                            8 => {
                                env.asm.push_str(&format!("    mov rax, {}\n", entry.addr.to_asm()));
                                env.asm.push_str("    push rax\n");
                            }
                            _fail => panic!("_fail = {}", _fail),
                        }
                    }
                    entry.ty.clone()
                } else {
                    panic!("token = {:?}", token);
                }
            }
            Atom::Paren { expr, .. } => expr.gen_asm_rvalue(env),
            Atom::Unit { .. } => Type::Unit,
            Atom::EnumValue {
                enum_ident,
                value_ident,
                ..
            } => {
                let enum_info = env
                    .type_table
                    .get_enum_member_info(&Type::UserType(enum_ident.s.clone()), &value_ident.s)
                    .expect(&format!("atom = {:?}", self));
                env.asm.push_str(&format!("    mov rax, {}\n", enum_info.value));
                env.asm.push_str("    push rax\n");
                Type::UserType(enum_ident.s.clone())
            }
            Atom::CharLiteral { token } => {
                let xs: Vec<char> = token.s.chars().collect();
                if xs.len() == 4 {
                    env.asm.push_str(&format!("    mov rax, {}\n", xs[2] as i64));
                    env.asm.push_str("    push rax\n");
                } else if xs.len() == 5 {
                    let v = match xs[3] {
                        'n' => '\n',
                        't' => '\t',
                        _ => xs[3],
                    };
                    env.asm.push_str(&format!("    mov rax, {}\n", v as i64));
                    env.asm.push_str("    push rax\n");
                } else {
                    unimplemented!();
                }
                Type::I(8)
            }
            Atom::StringLiteral { token } => {
                let label = env.gen_label();
                env.string_table.push(&label, &token);
                env.asm.push_str(&format!("    lea rax, {}\n", label));
                env.asm.push_str("    push rax\n");                
                Type::Ref(Box::new(Type::Array(Box::new(Type::I(8)), 1)))
            }
        }
    }
}

impl GenAsmExpr for RefApp {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        panic!("ref_app = {:?}", self);
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.prefix_unary_expr.gen_asm_lvalue(env);
        Type::Ref(Box::new(ty))
    }
}

impl GenAsmExpr for RefAppInArg {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        panic!("ref_app = {:?}", self);
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.argument.gen_asm_lvalue(env);
        Type::Ref(Box::new(ty))
    }
}

impl GenAsmExpr for DerefApp {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        let ty = self.prefix_unary_expr.gen_asm_rvalue(env);
        let ty = if let Type::Ref(ty2) = ty {
            *ty2.clone()
        } else {
            panic!("ty = {:?}", ty)
        };
        ty
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.prefix_unary_expr.gen_asm_rvalue(env);
        let ty: Type = if let Type::Ref(ty2) = ty {
            *ty2.clone()
        } else {
            panic!("ty = {:?}", ty)
        };
        env.asm.push_str("    pop rax\n");
        env.asm.push_str("    mov rax, [rax]\n");
        env.asm.push_str("    push rax\n");
        ty
    }
}

impl GenAsmExpr for DerefAppInArg {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        let ty = self.argument.gen_asm_rvalue(env);
        let ty = if let Type::Ref(ty2) = ty {
            *ty2.clone()
        } else {
            panic!("ty = {:?}", ty)
        };
        ty
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.argument.gen_asm_rvalue(env);
        let ty: Type = if let Type::Ref(ty2) = ty {
            *ty2.clone()
        } else {
            panic!("ty = {:?}", ty)
        };
        env.asm.push_str("    pop rax\n");
        env.asm.push_str("    mov rax, [rax]\n");
        env.asm.push_str("    push rax\n");
        ty
    }
}

impl GenAsmExpr for NegateApp {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        panic!("negate_app = {:?}", self);
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.prefix_unary_expr.gen_asm_rvalue(env);
        env.asm.push_str("    pop rdi\n");
        env.asm.push_str("    mov rax, 0\n");
        env.asm.push_str("    sub rax, rdi\n");
        env.asm.push_str("    push rax\n");
        ty
    }
}

impl GenAsmExpr for NegateAppInArg {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        panic!("negate_app = {:?}", self);
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.argument.gen_asm_rvalue(env);
        env.asm.push_str("    pop rdi\n");
        env.asm.push_str("    mov rax, 0\n");
        env.asm.push_str("    sub rax, rdi\n");
        env.asm.push_str("    push rax\n");
        ty
    }
}

impl GenAsmExpr for PrefixUnaryExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        match self {
            PrefixUnaryExpr::RefApp(ref_app) => ref_app.gen_asm_lvalue(env),
            PrefixUnaryExpr::DerefApp(deref_app) => deref_app.gen_asm_lvalue(env),
            PrefixUnaryExpr::NegateApp(negate_app) => negate_app.gen_asm_lvalue(env),
            PrefixUnaryExpr::FunApp(fun_app) => fun_app.gen_asm_lvalue(env),
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        match self {
            PrefixUnaryExpr::RefApp(ref_app) => ref_app.gen_asm_rvalue(env),
            PrefixUnaryExpr::DerefApp(deref_app) => deref_app.gen_asm_rvalue(env),
            PrefixUnaryExpr::NegateApp(negate_app) => negate_app.gen_asm_rvalue(env),
            PrefixUnaryExpr::FunApp(fun_app) => fun_app.gen_asm_rvalue(env),
        }
    }
}

impl GenAsmExpr for Argument {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        match self {
            Argument::RefAppInArg(ref_app) => ref_app.gen_asm_lvalue(env),
            Argument::DerefAppInArg(deref_app) => deref_app.gen_asm_lvalue(env),
            Argument::NegateAppInArg(negate_app) => negate_app.gen_asm_lvalue(env),
            Argument::PostfixUnaryExpr(postfix_unary_expr) => postfix_unary_expr.gen_asm_lvalue(env),
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        match self {
            Argument::RefAppInArg(ref_app) => ref_app.gen_asm_rvalue(env),
            Argument::DerefAppInArg(deref_app) => deref_app.gen_asm_rvalue(env),
            Argument::NegateAppInArg(negate_app) => negate_app.gen_asm_rvalue(env),
            Argument::PostfixUnaryExpr(postfix_unary_expr) => postfix_unary_expr.gen_asm_rvalue(env),
        }
    }
}

impl GenAsmExpr for IndexApp {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        let ty = self.postfix_unary_expr.gen_asm_dlvalue(env);
        match ty {
            Type::Array(ty, _num) => {
                let memsize = memsize_of_type(&ty, &env.type_table);
                let index_ty = self.expr.gen_asm_rvalue(env);
                if index_ty != Type::I(64) && index_ty != Type::Any {
                    panic!("index_ty = {:?}", index_ty);
                }
                env.asm.push_str("    pop rdi\n");
                env.asm.push_str(&format!("    mov rax, {}\n", memsize));
                env.asm.push_str("    imul rax, rdi\n");
                env.asm.push_str("    pop rdi\n");
                env.asm.push_str("    add rax, rdi\n");
                env.asm.push_str("    push rax\n");
                *ty.clone()
            }
            _ => panic!("ty = {:?}", ty),
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.postfix_unary_expr.gen_asm_dlvalue(env);
        match ty {
            Type::Array(ty, _num) => {
                let memsize = memsize_of_type(&ty, &env.type_table);
                let index_ty = self.expr.gen_asm_rvalue(env);
                if index_ty != Type::I(64) && index_ty != Type::Any {
                    panic!("index_ty = {:?}", index_ty);
                }
                env.asm.push_str("    pop rdi\n");
                env.asm.push_str(&format!("    mov rax, {}\n", memsize));
                env.asm.push_str("    imul rax, rdi\n");
                env.asm.push_str("    pop rdi\n");
                env.asm.push_str("    add rax, rdi\n");
                match memsize_of_type(&ty, &env.type_table) {
                    1 => {
                        env.asm.push_str("    mov al, [rax]\n");
                        env.asm.push_str("    movsx rax, al\n");
                    }
                    8 => {
                        env.asm.push_str("    mov rax, [rax]\n");
                    }
                    _fail => panic!("_fail = {:?}", _fail),
                }
                env.asm.push_str("    push rax\n");
                *ty.clone()
            }
            _ => panic!("ty = {:?}", ty),
        }
    }
}

impl GenAsmExpr for DotApp {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        let ty = self.postfix_unary_expr.gen_asm_dlvalue(env);
        let member_info = env
            .type_table
            .get_struct_member_info(&ty, &self.ident.s)
            .expect(&format!("dot_app = {:?}", self));
        env.asm.push_str("    pop rax\n");
        env.asm.push_str(&format!("    mov rdx, {}\n", member_info.offset));
        env.asm.push_str("    add rax, rdx\n");
        env.asm.push_str("    push rax\n");
        member_info.ty.clone()
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let ty = self.postfix_unary_expr.gen_asm_dlvalue(env);
        let member_info = env
            .type_table
            .get_struct_member_info(&ty, &self.ident.s)
            .expect(&format!("dot_app = {:?}", self));
        env.asm.push_str("    pop rax\n");
        env.asm.push_str(&format!("    mov rdx, {}\n", member_info.offset));
        env.asm.push_str("    add rax, rdx\n");
        env.asm.push_str("    mov rax, [rax]\n");
        env.asm.push_str("    push rax\n");
        member_info.ty.clone()
    }
}

impl GenAsmExpr for PostfixUnaryExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        match self {
            PostfixUnaryExpr::IndexApp(index_app) => index_app.gen_asm_lvalue(env),
            PostfixUnaryExpr::DotApp(dot_app) => dot_app.gen_asm_lvalue(env),
            PostfixUnaryExpr::Atom(atom) => atom.gen_asm_lvalue(env),
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        match self {
            PostfixUnaryExpr::IndexApp(index_app) => index_app.gen_asm_rvalue(env),
            PostfixUnaryExpr::DotApp(dot_app) => dot_app.gen_asm_rvalue(env),
            PostfixUnaryExpr::Atom(atom) => atom.gen_asm_rvalue(env),
        }
    }
}

impl GenAsmExpr for FunApp {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        if self.args.is_empty() {
            self.fun.gen_asm_lvalue(env)
        } else {
            panic!("fun_app = {:?}", self);
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        if self.args.is_empty() {
            self.fun.gen_asm_rvalue(env)
        } else {
            let mut arg_types = vec![];
            for arg in &self.args {
                let arg_ty = arg.gen_asm_rvalue(env);
                arg_types.push(arg_ty);
            }
            let mut ty = self.fun.gen_asm_rvalue(env);
            env.asm.push_str("    pop rax\n");
            env.asm.push_str("    call rax\n");
            for arg_ty in arg_types {
                ty = Type::app(&ty, &arg_ty).expect(&format!("fun_app = {:?}, ty = {:?}, arg_ty = {:?}", self, ty, arg_ty));
                if arg_ty != Type::Unit {
                    env.asm.push_str("    pop r15\n");
                }
            }
            env.asm.push_str("    push rax\n");
            if let Type::Arrow(_,_) = ty {
                panic!("not fully applied: fun_app = {:?}", self);
            }
            ty
        }
    }
}

impl GenAsmExpr for LoopExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        panic!("loop_expr = {:?}", self);
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let begin = env.gen_label();
        let end = env.gen_label();
        env.asm.push_str(&format!("{}:\n", begin));
        env.push_loop_label(&begin, &end);
        self.block_expr.gen_asm_rvalue(env);
        env.asm.push_str(&format!("     jmp {}\n", begin));
        env.asm.push_str(&format!("{}:\n", end));
        env.pop_loop_label();
        Type::Unit
    }
}

impl GenAsmExpr for WhileExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        panic!("while_expr = {:?}", self);
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let begin = env.gen_label();
        let end = env.gen_label();
        env.asm.push_str(&format!("{}:\n", begin));
        let _ty = self.cond_expr.gen_asm_rvalue(env);
        env.asm.push_str("    pop rax\n");
        env.asm.push_str("    mov rdi, 0\n");
        env.asm.push_str("    cmp rax, rdi\n");
        env.asm.push_str(&format!("    je {}\n", end));
        env.push_loop_label(&begin, &end);
        self.block_expr.gen_asm_rvalue(env);
        env.pop_loop_label();
        env.asm.push_str(&format!("     jmp {}\n", begin));
        env.asm.push_str(&format!("{}:\n", end));
        Type::Unit
    }
}

impl GenAsmExpr for IfExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        let label1 = env.gen_label();
        let label2 = env.gen_label();
        self.expr1.gen_asm_rvalue(env);
        env.asm.push_str("    pop rax\n");
        env.asm.push_str("    cmp rax, 0\n");
        env.asm.push_str(&format!("    je {}\n", label1));
        let ty2 = self.expr2.gen_asm_lvalue(env);
        env.asm.push_str(&format!("    jmp {}\n", label2));
        env.asm.push_str(&format!("{}:\n", label1));
        let ty3 = self.expr3.gen_asm_lvalue(env);
        env.asm.push_str(&format!("{}:\n", label2));
        Type::merge(&ty2, &ty3).expect(&format!("if_expr = {:?}, ty2 = {:?}, ty3 = {:?}", self, ty2, ty3))
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        let label1 = env.gen_label();
        let label2 = env.gen_label();
        self.expr1.gen_asm_rvalue(env);
        env.asm.push_str("    pop rax\n");
        env.asm.push_str("    cmp rax, 0\n");
        env.asm.push_str(&format!("    je {}\n", label1));
        let ty2 = self.expr2.gen_asm_rvalue(env);
        env.asm.push_str(&format!("    jmp {}\n", label2));
        env.asm.push_str(&format!("{}:\n", label1));
        let ty3 = self.expr3.gen_asm_rvalue(env);
        env.asm.push_str(&format!("{}:\n", label2));
        Type::merge(&ty2, &ty3).expect(&format!("if_expr = {:?}, ty2 = {:?}, ty3 = {:?}", self, ty2, ty3))
    }
}

impl GenAsmExpr for BinaryApp {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        panic!("binary_app = {:?}", self);
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        if self.op.s == "+" || self.op.s == "-" || self.op.s == "*" || self.op.s == "/" || self.op.s == "%" {
            let ty1 = self.left.gen_asm_rvalue(env);
            let ty2 = self.right.gen_asm_rvalue(env);
            env.asm.push_str("    pop rdi\n");
            env.asm.push_str("    pop rax\n");
            match (&ty1,&ty2) {
                // TODO: remove (i8,any), (any,i8), (usertype, usertype)
                (Type::I(8), Type::I(8)) | (Type::I(8), Type::Any) | (Type::Any, Type::I(8)) => {
                    if self.op.s == "+" {
                        panic!("binary_app = {:?}", self);
                    } else if self.op.s == "-" {
                        panic!("binary_app = {:?}", self);
                    } else if self.op.s == "*" {
                        panic!("binary_app = {:?}", self);
                    } else if self.op.s == "/" {
                        panic!("binary_app = {:?}", self);
                    } else if self.op.s == "%" {
                        panic!("binary_app = {:?}", self);
                    } else {
                        unreachable!();
                    }
                }
                (Type::I(64), Type::I(64)) | (Type::Any, Type::Any) | (Type::I(64), Type::Any) | (Type::Any, Type::I(64)) 
                | (Type::Ref(_), Type::Any) | (Type::Any, Type::Ref(_)) | (Type::Ref(_), Type::Ref(_)) | (Type::UserType(_), Type::UserType(_)) => {
                    //let ty1 = self.left.gen_asm_rvalue(env);
                    //let ty2 = self.right.gen_asm_rvalue(env);
                    //env.asm.push_str("    pop rdi\n");
                    //env.asm.push_str("    pop rax\n");
                            if self.op.s == "+" {
                        env.asm.push_str("    add rax, rdi\n");
                        env.asm.push_str("    push rax\n");
                        Type::I(64)
                    } else if self.op.s == "-" {
                        env.asm.push_str("    sub rax, rdi\n");
                        env.asm.push_str("    push rax\n");
                        Type::I(64)
                    } else if self.op.s == "*" {
                        env.asm.push_str("    imul rax, rdi\n");
                        env.asm.push_str("    push rax\n");
                        Type::I(64)
                    } else if self.op.s == "/" {
                        env.asm.push_str("    cqo\n");
                        env.asm.push_str("    idiv rdi\n");
                        env.asm.push_str("    push rax\n");
                        Type::I(64)
                    } else if self.op.s == "%" {
                        env.asm.push_str("    cqo\n");
                        env.asm.push_str("    idiv rdi\n");
                        env.asm.push_str("    mov rax, rdx\n");
                        env.asm.push_str("    push rax\n");
                        Type::I(64)
                    } else {
                        unreachable!();
                    }
                }
                _ => panic!("binary_app: mismatch type, ty1 = {:?}, ty2: {:?}, binary_app = {:?}", ty1, ty2, self),
            }
        } else if self.op.s == "==" || self.op.s == "!=" || self.op.s == "<" || self.op.s == "<=" || self.op.s == ">" || self.op.s == ">=" {
            let ty1 = self.left.gen_asm_rvalue(env);
            let ty2 = self.right.gen_asm_rvalue(env);
            env.asm.push_str("    pop rdi\n");
            env.asm.push_str("    pop rax\n");
            let label1 = env.gen_label();
            let label2 = env.gen_label();
            let ty = Type::merge(&ty1, &ty2);
            if ty == Ok(Type::I(8)) {
                env.asm.push_str("    cmp al, dil\n");
            } else if ty == Ok(Type::I(64)) || ty == Ok(Type::Any) || (ty.is_ok() && ty.unwrap().is_user_type()) {
                env.asm.push_str("    cmp rax, rdi\n");                
            } else {
                panic!("ty1 = {:?}, ty2 = {:?}", ty1, ty2);
            }
            if self.op.s == "==" {
                env.asm.push_str(&format!("    je {}\n", label1));
            } else if self.op.s == "!=" {
                env.asm.push_str(&format!("    jne {}\n", label1));
            } else if self.op.s == "<" {
                env.asm.push_str(&format!("    jl {}\n", label1));
            } else if self.op.s == "<=" {
                env.asm.push_str(&format!("    jle {}\n", label1));
            } else if self.op.s == ">" {
                env.asm.push_str(&format!("    jg {}\n", label1));
            } else if self.op.s == ">=" {
                env.asm.push_str(&format!("    jge {}\n", label1));
            } else {
                panic!("binary_app = {:?}", self);
            }
            env.asm.push_str("    mov rax, 0\n");
            env.asm.push_str(&format!("    jmp {}\n", label2));
            env.asm.push_str(&format!("{}:\n", label1));
            env.asm.push_str("    mov rax, 1\n");
            env.asm.push_str(&format!("{}:\n", label2));
            env.asm.push_str("    push rax\n");
            Type::I(64)
        } else if self.op.s == "&&" || self.op.s == "||" {
            let label1 = env.gen_label();
            let label2 = env.gen_label();
            let ty1 = self.left.gen_asm_rvalue(env);
            env.asm.push_str("    pop rax\n");
            env.asm.push_str("    mov rdi, 0\n");
            env.asm.push_str("    cmp rax, rdi\n");
            if self.op.s == "&&" {
                env.asm.push_str(&format!("    jne {}\n", label1));
                env.asm.push_str("    mov rax, 0\n");
            } else if self.op.s == "||" {
                env.asm.push_str(&format!("    je {}\n", label1));
                env.asm.push_str("    mov rax, 1\n");    
            } else {
                unreachable!();
            }
            env.asm.push_str("    push rax\n");
            env.asm.push_str(&format!("    jmp {}\n", label2));
            env.asm.push_str(&format!("{}:\n", label1));
            let ty2 = self.right.gen_asm_rvalue(env);
            env.asm.push_str(&format!("{}:\n", label2));
            assert_eq!(ty1, ty2);
            Type::I(64)
        } else {
            panic!();
        }
    }
}

impl GenAsmExpr for Term {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        match self {
            Term::PrefixUnaryExpr(prefix_unary_expr) => prefix_unary_expr.gen_asm_lvalue(env),
            Term::IfExpr(if_expr) => if_expr.gen_asm_lvalue(env),
            Term::LoopExpr(loop_expr) => loop_expr.gen_asm_lvalue(env),
            Term::WhileExpr(while_expr) => while_expr.gen_asm_lvalue(env),
            Term::MatchExpr(match_expr) => match_expr.gen_asm_lvalue(env),
            Term::BlockExpr(block_expr) => block_expr.gen_asm_lvalue(env),
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        match self {
            Term::PrefixUnaryExpr(prefix_unary_expr) => prefix_unary_expr.gen_asm_rvalue(env),
            Term::IfExpr(if_expr) => if_expr.gen_asm_rvalue(env),
            Term::LoopExpr(loop_expr) => loop_expr.gen_asm_rvalue(env),
            Term::WhileExpr(while_expr) => while_expr.gen_asm_rvalue(env),
            Term::MatchExpr(match_expr) => match_expr.gen_asm_rvalue(env),
            Term::BlockExpr(block_expr) => block_expr.gen_asm_rvalue(env),
        }        
    }
}

impl GenAsmExpr for Expr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        match self {
            Expr::BinaryApp(_) => {
                panic!("expr = {:?}", self);
            }
            Expr::Term(term) => {
                term.gen_asm_lvalue(env)
            }
        }
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        match self {
            Expr::BinaryApp(binary_app) => binary_app.gen_asm_rvalue(env),
            Expr::Term(term) => {
                term.gen_asm_rvalue(env)
            }
        }
    }
}

impl GenAsmExpr for MatchExpr {
    fn gen_asm_lvalue(&self, env: &mut Env) -> Type {
        self.expr.gen_asm_rvalue(env);
        env.asm.push_str("    pop rax\n");
        let final_label = env.gen_label();
        let mut res_ty = None;
        for pattern in &self.patterns {
            let next = env.gen_label();
            let enum_info = env
                .type_table
                .get_enum_member_info(
                    &Type::UserType(pattern.enum_ident.s.clone()),
                    &pattern.value_ident.s,
                )
                .unwrap();
            env.asm.push_str(&format!("    mov rdx, {}\n", enum_info.value));
            env.asm.push_str("    cmp rax, rdx\n");
            env.asm.push_str(&format!("    jne {}\n", next));
            let ty = pattern.expr.gen_asm_lvalue(env);
            res_ty = Some(ty);
            env.asm.push_str(&format!("    jmp {}\n", final_label));
            env.asm.push_str(&format!("{}:\n", next));
        }
        // TODO: no-match panic
        env.asm.push_str(&format!("{}:\n", final_label));
        res_ty.unwrap()
    }
    fn gen_asm_rvalue(&self, env: &mut Env) -> Type {
        self.expr.gen_asm_rvalue(env);
        env.asm.push_str("    pop rax\n");
        let final_label = env.gen_label();
        let mut res_ty = None;
        for pattern in &self.patterns {
            let next = env.gen_label();
            let enum_info = env
                .type_table
                .get_enum_member_info(
                    &Type::UserType(pattern.enum_ident.s.clone()),
                    &pattern.value_ident.s,
                )
                .expect(&format!("match_expr = {:?}", self));
            env.asm.push_str(&format!("    mov rdx,  {}\n", enum_info.value));
            env.asm.push_str("    cmp rax, rdx\n");
            env.asm.push_str(&format!("    jne {}\n", next));
            let ty = pattern.expr.gen_asm_rvalue(env);
            res_ty = Some(ty);
            env.asm.push_str(&format!("    jmp {}\n", final_label));
            env.asm.push_str(&format!("{}:\n", next));
        }
        // TODO: no-match panic
        env.asm.push_str(&format!("{}:\n", final_label));
        res_ty.unwrap()
    }
}
