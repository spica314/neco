use crate::syntax::*;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Type {
    Unit,
    I(i64),
    Arrow(Box<Type>, Box<Type>),
    Array(Box<Type>, i64),
    Ref(Box<Type>),
    UserType(String),
    AnyPtr,
    Any,
}

impl Type {
    pub fn is_ref(&self) -> bool {
        if let Type::Ref(_) = self {
            true
        } else {
            false
        }
    }
    pub fn is_integer(&self) -> bool {
        if let Type::I(_) = self {
            true
        } else {
            false
        }
    }
    pub fn is_user_type(&self) -> bool {
        if let Type::UserType(_) = self {
            true
        } else {
            false
        }
    }
    pub fn merge(ty1: &Type, ty2: &Type) -> Result<Type, ()> {
        if let Type::Any = ty1 {
            Ok(ty2.clone())
        } else if let Type::Any = ty2 {
            Ok(ty1.clone())
        } else if ty1 == ty2 {
            Ok(ty1.clone())
        } else {
            Err(())
        }
    }
    pub fn from_syntype(syn_type: &SynType) -> Type {
        match syn_type {
            SynType::PrimitiveType(primitive_type) => {
                if primitive_type.token.s.starts_with('i') {
                    let n_bits = primitive_type.token.s[1..]
                        .parse::<i64>()
                        .unwrap_or_else(|_| panic!("syn_type = {:?}", syn_type));
                    Type::I(n_bits)
                } else {
                    Type::UserType(primitive_type.token.s.clone())
                }
            }
            SynType::SynArrayType(syn_array_type) => {
                let ty = &syn_array_type.ty;
                let number = &syn_array_type.number;
                Type::Array(
                    Box::new(Type::from_syntype(&ty)),
                    number.s.parse::<i64>().unwrap(),
                )
            }
            SynType::RefType(ref_type) => {
                let ty = Type::from_syntype(&ref_type.ty);
                Type::Ref(Box::new(ty))
            }
            SynType::UnitType(_) => Type::Unit,
            SynType::AnyPtrType(_) => Type::AnyPtr,
            SynType::AnyType(_) => Type::Any,
        }
    }
    pub fn assignable(ty1: &Type, ty2: &Type) -> bool {
        if &Type::Any == ty1 || &Type::Any == ty2 || &Type::AnyPtr == ty1 || &Type::AnyPtr == ty2 {
            return true;
        }
        match ty1 {
            Type::Unit if ty2 == &Type::Unit => true,
            Type::I(_) if ty1 == ty2 => true,
            Type::Arrow(_, _) if ty1 == ty2 => true,
            Type::Array(e1, _) => {
                if let Type::Array(e2, _) = ty2 {
                    e1 == e2
                } else {
                    false
                }
            }
            Type::Ref(a) => {
                if let Type::Ref(b) = ty2 {
                    Type::assignable(a, b)
                } else {
                    false
                }
            }
            Type::UserType(_) if ty1 == ty2 => true,
            Type::AnyPtr if ty2.is_ref() || ty2 == &Type::AnyPtr => true,
            Type::Any
                if ty2 == &Type::AnyPtr
                    || ty2 == &Type::Any
                    || ty2 == &Type::I(64)
                    || ty2.is_ref() =>
            {
                true
            }
            _ => false,
        }
    }
    pub fn app(ty1: &Type, ty2: &Type) -> Result<Type, ()> {
        match ty1 {
            Type::Arrow(ty3, ty4) => {
                if Type::assignable(&ty3, ty2) {
                    Ok(*ty4.clone())
                } else {
                    Err(())
                }
            }
            _ => Err(()),
        }
    }
}
