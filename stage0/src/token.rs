#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum TokenKind {
    Number,
    IDENT,
    CHAR,
    STRING,
    LPAREN,
    RPAREN,
    LBRACE,
    RBRACE,
    LBRACKET,
    RBRACKET,
    COLON,
    FN,
    ARROW,
    LET,
    MUT,
    EQUAL,
    SEMICOLON,
    UNIT,
    IF,
    ELSE,
    BREAK,
    CONTINUE,
    LOOP,
    STRUCT,
    COMMA,
    DOT,
    MATCH,
    ARROW2,
    COLON2,
    ENUM,
    THEN,
    ANYPTR,
    ANY,
    RETURN,
    REF,
    DEREF,
    NEG,
    EQ,
    NEQ,
    GT,
    GEQ,
    LT,
    LEQ,
    AND,
    OR,
    NOT,
    ADD,
    SUB,
    MUL,
    DIV,
    MOD,
    WHILE,
}

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct Token {
    pub s: String,
    pub t: TokenKind,
    pub line: usize,
}

const KEYWORDS: &[(&str, TokenKind)] = &[
    ("continue", TokenKind::CONTINUE),
    ("any_ptr", TokenKind::ANYPTR),
    ("struct", TokenKind::STRUCT),
    ("return", TokenKind::RETURN),
    ("match", TokenKind::MATCH),
    ("break", TokenKind::BREAK),
    ("while", TokenKind::WHILE),
    ("loop", TokenKind::LOOP),
    ("enum", TokenKind::ENUM),
    ("then", TokenKind::THEN),
    ("else", TokenKind::ELSE),
    ("any", TokenKind::ANY),
    ("let", TokenKind::LET),
    ("mut", TokenKind::MUT),
    ("fn", TokenKind::FN),
    ("()", TokenKind::UNIT),
    ("if", TokenKind::IF),
    ("->", TokenKind::ARROW),
    ("=>", TokenKind::ARROW2),
    ("::", TokenKind::COLON2),
    ("&&", TokenKind::AND),
    ("||", TokenKind::OR),
    ("==", TokenKind::EQ),
    ("!=", TokenKind::NEQ),
    ("<=", TokenKind::LEQ),
    (">=", TokenKind::GEQ),
    ("<", TokenKind::LT),
    (">", TokenKind::GT),
    ("+", TokenKind::ADD),
    ("-", TokenKind::SUB),
    ("*", TokenKind::MUL),
    ("/", TokenKind::DIV),
    ("%", TokenKind::MOD),
    ("&", TokenKind::AND),
    ("!", TokenKind::NOT),
    ("(", TokenKind::LPAREN),
    (")", TokenKind::RPAREN),
    ("{", TokenKind::LBRACE),
    ("}", TokenKind::RBRACE),
    ("[", TokenKind::LBRACKET),
    ("]", TokenKind::RBRACKET),
    (":", TokenKind::COLON),
    ("=", TokenKind::EQUAL),
    (";", TokenKind::SEMICOLON),
    (",", TokenKind::COMMA),
    (".", TokenKind::DOT),
];

fn match_keyword(s: &[char], i: usize, line: usize) -> Option<(Token, usize)> {
    'a: for (keyword_s, keyword_t) in KEYWORDS {
        let mut p = i;
        let mut maybe_ident = false;
        for (k, c) in keyword_s.chars().enumerate() {
            if k == 0 && (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '_') {
                maybe_ident = true;
            }
            if i + k >= s.len() || s[i + k] != c {
                continue 'a;
            }
            p = i + k + 1;
        }
        if maybe_ident
            && (('a' <= s[p] && s[p] <= 'z') || ('A' <= s[p] && s[p] <= 'Z') || s[p] == '_')
        {
            return None;
        }
        return Some((
            Token {
                s: (*keyword_s).to_string(),
                t: *keyword_t,
                line,
            },
            p,
        ));
    }
    None
}

const PREFIX_KEYWORDS: &[(char, TokenKind)] = &[
    ('&', TokenKind::REF),
    ('*', TokenKind::DEREF),
    ('-', TokenKind::NEG),
];

fn match_prefix_operator(s: &[char], i: usize, line: usize) -> Option<(Vec<Token>, usize)> {
    if i == 0 {
        return None;
    }
    if s[i-1] != ' ' {
        return None;
    } 
    let mut k = i;
    let mut res = vec![];
    'a: loop {
        for prefix_keyword in PREFIX_KEYWORDS {
            if s[k] == prefix_keyword.0 {
                res.push(Token {
                    s: s[k].to_string(),
                    t: prefix_keyword.1,
                    line,
                });
                k = k + 1;
                continue 'a;
            }
        }
        if s[k].is_whitespace() {
            return None;
        }
        break;
    }
    if res.is_empty() {
        None
    } else {
        Some((res, k))
    }
}

impl Token {
    pub fn lex(s: &str) -> Vec<Token> {
        let cs: Vec<char> = s.chars().collect();
        let mut res = vec![];
        let mut i = 0;
        let n = cs.len();
        let mut line = 1;
        while i < n {
            // space
            while i < n && (cs[i] == ' ' || cs[i] == '\n') {
                if cs[i] == '\n' {
                    line += 1;
                }
                i += 1;
            }
            if i >= n {
                break;
            }
            // one-line comment
            if i < n && cs[i] == '/' && i + 1 < n && cs[i + 1] == '/' {
                while i < n && cs[i] != '\n' {
                    i += 1;
                }
                continue;
            }
            // multi-line comment
            if i + 1 < n && cs[i] == '/' && cs[i + 1] == '*' {
                i += 2;
                while i + 1 < n && !(cs[i] == '*' && cs[i + 1] == '/') {
                    if cs[i] == '\n' {
                        line += 1;
                    }
                    i += 1;
                }
                i += 2;
                continue;
            }
            // prefix operator ('&', '*', '-')
            if let Some((ts, p)) = match_prefix_operator(&cs, i, line) {
                for t in ts {
                    res.push(t);
                }
                i = p;
                continue;
            }
            // keyword
            if let Some((t, p)) = match_keyword(&cs, i, line) {
                res.push(t);
                i = p;
                continue;
            }
            // char
            if i + 1 < cs.len() && cs[i] == 'b' && cs[i + 1] == '\'' {
                let mut s = String::new();
                s.push(cs[i]);
                i += 1;
                s.push(cs[i]);
                i += 1;
                loop {
                    if cs[i] == '\\' {
                        s.push(cs[i]);
                        i += 1;
                        s.push(cs[i]);
                        i += 1;
                    } else {
                        s.push(cs[i]);
                        i += 1;
                        if cs[i - 1] == '\'' {
                            break;
                        }
                    }
                }
                res.push(Token {
                    s,
                    t: TokenKind::CHAR,
                    line,
                });
                continue;
            }
            // string
            if i + 1 < cs.len() && cs[i] == 'b' && cs[i + 1] == '\"' {
                let mut s = String::new();
                s.push(cs[i]);
                i += 1;
                s.push(cs[i]);
                i += 1;
                loop {
                    if cs[i] == '\\' {
                        s.push(cs[i]);
                        i += 1;
                        s.push(cs[i]);
                        i += 1;
                    } else {
                        s.push(cs[i]);
                        i += 1;
                        if cs[i - 1] == '\"' {
                            break;
                        }
                    }
                }
                res.push(Token {
                    s,
                    t: TokenKind::STRING,
                    line,
                });
                continue;
            }
            // num
            if cs[i].is_numeric() {
                let mut s = String::new();
                while i < n && cs[i].is_numeric() {
                    s.push(cs[i]);
                    i += 1;
                }
                res.push(Token {
                    s,
                    t: TokenKind::Number,
                    line,
                });
                continue;
            }
            // ident
            if cs[i].is_ascii_alphabetic() || cs[i] == '_' {
                let mut s = String::new();
                s.push(cs[i]);
                i += 1;
                while i < n && (cs[i].is_ascii_alphanumeric() || cs[i] == '_') {
                    s.push(cs[i]);
                    i += 1;
                }
                res.push(Token {
                    s,
                    t: TokenKind::IDENT,
                    line,
                });
                continue;
            }
            panic!("i = {}, rem = {:?}", i, &cs[i..]);
        }
        res
    }
}
