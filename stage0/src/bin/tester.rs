use glob::glob;
use std::path::Path;
use std::process::Command;

fn test(path: &Path) {
    eprintln!("test: {:?}", path);
    let txt = std::fs::read_to_string(&path).unwrap();
    let first_line = txt.lines().next().unwrap();
    if first_line.starts_with("// ") {
        let expect_code = first_line[3..].parse::<i32>().unwrap();
        let status = Command::new("cargo")
            .args(&[
                "run",
                "--bin",
                "neco",
                "workspace/a.s",
                path.to_str().unwrap(),
            ])
            .status()
            .expect("");
        assert_eq!(status.code(), Some(0));
        let status = Command::new("as")
            .args(&["-o", "workspace/a.o", "workspace/a.s"])
            .status()
            .expect("");
        assert_eq!(status.code(), Some(0));
        let status = Command::new("ld")
            .args(&["-o", "workspace/a.out", "workspace/a.o"])
            .status()
            .expect("");
        assert_eq!(status.code(), Some(0));
        let status = Command::new("./workspace/a.out").status().expect("");
        assert_eq!(status.code().unwrap(), expect_code);
    }
    println!("{:?}: ok", path);
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() == 1 {
        let query = "./test_codes/test_*.neco";
        for entry in glob(query).expect("") {
            match entry {
                Ok(path) => {
                    test(&path);
                }
                Err(e) => {
                    println!("{:?}", e);
                }
            }
        }
    } else {
        let query = &format!("./test_codes/test_{}.neco", args[1]);
        let path = Path::new(&query);
        test(&path);
    }
}
