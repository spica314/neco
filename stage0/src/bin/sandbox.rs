use stage0::asm::*;
use stage0::syntax::*;
use stage0::token::*;
use std::io::Read;

fn main() {
    let stdin = std::io::stdin();
    let mut input = String::new();
    stdin.lock().read_to_string(&mut input).unwrap();
    let s = input.as_str();
    let ts = Token::lex(&s);
    eprintln!("{:?}", ts);
    let program = parse_program(&ts, &mut 0);
    eprintln!("{:?}", program);
    let asm = gen_asm_program(&program.unwrap());
    println!("{}", asm);
}
