use stage0::asm::*;
use stage0::syntax::*;
use stage0::token::*;
use std::fs::File;
use std::io::Write;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() == 3 {
        let s = std::fs::read_to_string(&args[2]).unwrap();
        let ts = Token::lex(&s);
        eprintln!("lex ok");
        let mut i = 0;
        let program = parse_program(&ts, &mut i).unwrap();
        if i != ts.len() {
            eprintln!("parse error");
            eprintln!("rem = {:?}", &ts[i..]);
            panic!();
        }
        eprintln!("parse ok");
        let asm = gen_asm_program(&program);

        let buf: Vec<u8> = asm.bytes().collect();
        let mut file = File::create(&args[1]).unwrap();
        file.write_all(&buf).unwrap();
        file.flush().unwrap();
    }
}
