#!/bin/bash

cargo run --bin neco workspace/main.s ../stage1/main.neco
if [ $? -ne 0 ]; then
    echo "failed"
    exit 1
fi

as -o workspace/main.o workspace/main.s
if [ $? -ne 0 ]; then
    echo "failed"
    exit 1
fi

ld -o ../bin/neco_stage1_1 workspace/main.o
if [ $? -ne 0 ]; then
    echo "failed"
    exit 1
fi

echo "ok"
