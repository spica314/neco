#!/bin/bash

mkdir -p workspace

cargo test
if [ $? -ne 0 ]; then
    echo "failed"
    exit 1
fi

RUST_BACKTRACE=1 cargo run --bin tester
if [ $? -ne 0 ]; then
    echo "failed"
    exit 1
fi

echo "ok"
